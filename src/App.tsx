import './css/App.scss';
import Navbar from './layout/Navbar';
import Gallery from './pages/Gallery';
import { Hero } from './pages/Hero';
import Introduction from './pages/Introduction';
import Menu from './pages/Menu';
import Quote from './pages/Quote';
import Location from './pages/Location';
import Footer from './layout/Footer';

function App() {
    return (
        <div className='App'>
            <Navbar />
            <Hero />
            <Introduction />
            <Quote />
            <Gallery />
            <Menu />
            <Location />
            <Footer />
        </div>
    );
}

export default App;
