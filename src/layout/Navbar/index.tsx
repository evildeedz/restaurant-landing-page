import { FC, useMemo, useState } from 'react';
import { IoRestaurantOutline } from 'react-icons/io5';
import { GiHamburgerMenu } from 'react-icons/gi';

interface INavbarProps {}

const Navbar: FC<INavbarProps> = (props) => {
    const [menuClicked, setMenuClicked] = useState(false);

    const links = useMemo(
        () => [
            { link: 'Introduction', id: 'introduction' },
            { link: 'Gallery', id: 'gallery' },
            { link: 'Location', id: 'location' },
        ],
        []
    );

    // Scroll
    const scrollTo = (id: string) => {
        document
            .getElementById(id)
            ?.scrollIntoView({ behavior: 'smooth', block: 'start' });
    };

    return (
        <header>
            <div className='container'>
                <div className='icon-container'>
                    <IoRestaurantOutline className='icon' />
                </div>
                <div className='links'>
                    {links.map(({ link, id }, index) => (
                        <span key={index} className='link' onClick={() => scrollTo(id)}>
                            {link}
                        </span>
                    ))}
                </div>
                <button className='menu-btn' onClick={() => scrollTo('menu')}>
                    MENU
                </button>
                <GiHamburgerMenu
                    className='burger-icon'
                    onClick={() => setMenuClicked(true)}
                />
            </div>
            {menuClicked && (
                <div className='mobile-links'>
                    <span className='close' onClick={() => setMenuClicked(false)}>
                        X
                    </span>
                    <div className='mobile-links-container'>
                        {links.map(({ link, id }, index) => (
                            <span
                                key={index}
                                className='mobile-link'
                                onClick={() => scrollTo(id)}
                            >
                                {link}
                            </span>
                        ))}
                    </div>
                    <button className='menu-btn' onClick={() => scrollTo('menu')}>
                        MENU
                    </button>
                </div>
            )}
        </header>
    );
};

export default Navbar;
