import { FC } from 'react';

interface IFooterProps {}

const Footer: FC<IFooterProps> = (props) => {
    return (
        <footer>
            <div className='container'>
                <div className='about-contact'>
                    <div className='about'>
                        <span>About Us</span>
                        <p>
                            Richard McClintock , a Latin professor at Hampden-Sydney
                            College in Virginia, looked up one of the more obscure Latin
                            words, consectetur, from a Lorem Ipsum passage, and going
                            through the cites of the word in classical literature,
                            discovered the undoubtable source.
                        </p>
                    </div>
                    <hr />
                    <div className='contact'>
                        <span>Contact Us</span>
                        <p>
                            100 Mainstreet Blvd. Miami hgi oflyt <br />
                            100 Mainstreet Blvd. Miami hgi oflyt <br />
                            Phone: (305) 555-4446 <br /> Phone: (305) 555-5896 <br /> Fax:
                            (305) 555-4447 <br /> E-Mail: johndoe@example.com <br />
                            Web Site: lipsum.co
                        </p>
                    </div>
                    <hr />
                    <div className='links'>
                        <span>Links</span>
                        <div className='links-container'>
                            <a href='#'>Link1</a>
                            <a href='#'>Link2</a>
                            <a href='#'>Link3</a>
                            <a href='#'>Link3</a>
                            <a href='#'>Link4</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
