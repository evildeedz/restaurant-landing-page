const gallery = [
    {
        src: 'https://images.unsplash.com/photo-1499715217757-2aa48ed7e593?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwcm9maWxlLXBhZ2V8MXx8fGVufDB8fHx8&w=1000&q=80',
        imgTitle: 'Lorem Ipsum',
        subtitle: 'Dolor',
    },
    {
        src: 'https://i.pinimg.com/originals/84/60/37/8460372aec15597e4b9d83944dba6048.jpg',
        imgTitle: 'Lorem Ipsum',
        subtitle: 'Dolor',
    },
    {
        src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQrAi8IKP-0B9Pjb_jTYeFfNSz7_QoUmHTJvMQdoA90uX0M3y5ICO-eC6pznXPjjCTX7i4&usqp=CAU',
        imgTitle: 'Lorem Ipsum',
        subtitle: 'Dolor',
    },
    {
        src: 'https://img.weyesimg.com/collection/b7f41fc1412ad2ee75e9b2635d3b9d5c/43b72706a5c3e5e4c5af9a4606dd70ec.jpg?imageView2/2/w/1920/q/100',
        imgTitle: 'Lorem Ipsum',
        subtitle: 'Dolor',
    },
    {
        src: 'https://i.pinimg.com/originals/a6/76/36/a67636af0cd4233eefcd8ce34a48c43e.jpg',
        imgTitle: 'Lorem Ipsum',
        subtitle: 'Dolor',
    },
    {
        src: 'https://assets.bonappetit.com/photos/57d6f68d1844fc374614321e/master/pass/ceramics-christopher-kostow-bowl.jpg',
        imgTitle: 'Lorem Ipsum',
        subtitle: 'Dolor',
    },
];

export default gallery;
