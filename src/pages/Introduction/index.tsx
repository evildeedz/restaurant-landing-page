import { FC } from 'react';
import { IoRestaurantOutline } from 'react-icons/io5';
import { GiVineFlower } from 'react-icons/gi';

interface IIntroductionProps {}

const Introduction: FC<IIntroductionProps> = (props) => {
    return (
        <section id='introduction'>
            <div className='container'>
                <div className='icon-container'>
                    <IoRestaurantOutline className='icon' />
                </div>{' '}
                <h2>Introduction</h2>
                <h4>Delicious Foods & Cosy Place</h4>
                <GiVineFlower className='flower' />
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
                    lobortis mauris ex, quis rutrum augue eleifend sed. Class aptent
                    taciti sociosqu ad litora torquent per conubia nostra, per inceptos
                    himenaeos. Maecenas in purus dui. Donec sed neque elit. Suspendisse
                    quis est porttitor, efficitur felis ut, facilisis leo. Maecenas lorem
                    nulla, aliquam non ex nec, gravida pharetra urna. Morbi tempus
                    sollicitudin diam a scelerisque. Praesent eget risus id odio
                    ullamcorper feugiat.
                </p>
            </div>
        </section>
    );
};

export default Introduction;
