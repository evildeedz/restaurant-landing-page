import { FC } from 'react';
import heroImg from '../../assets/images/hero.jpg';
const Slide = require('react-reveal/Slide');

interface IHeroProps {}

export const Hero: FC<IHeroProps> = (props) => {
    return (
        <section id='hero'>
            <img src={heroImg} alt='hero_img' />
            <Slide duratioon={2000} bottom>
                <div className='container'>
                    <h3>Luxury Cuisine</h3>
                    <h1>Restaurant</h1>
                    <button
                        className='cta'
                        onClick={() =>
                            document
                                .getElementById('introduction')
                                ?.scrollIntoView({ behavior: 'smooth', block: 'start' })
                        }
                    >
                        READ MORE
                    </button>
                </div>
            </Slide>
        </section>
    );
};
