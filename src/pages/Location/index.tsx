import { FC } from 'react';
import { GiForkKnifeSpoon } from 'react-icons/gi';
import { GoogleMap, useJsApiLoader } from '@react-google-maps/api';

interface ILocationProps {}
const center = {
    lat: 46.1157,
    lng: -85.1732,
};
const containerStyle = {
    width: '100%',
    height: '100%',
};

console.log(process.env.REACT_APP_MAP_API_KEY);

const Location: FC<ILocationProps> = (props) => {
    const { isLoaded } = useJsApiLoader({
        id: 'google-map-script',
        googleMapsApiKey: `${process.env.REACT_APP_MAP_API_KEY}`,
    });

    return (
        <section id='location'>
            <div className='container'>
                <div className='text'>
                    <GiForkKnifeSpoon className='cutlery-icon' />
                    <span className='title'>Location</span>
                    <h4>We would love to have you</h4>
                    <span className='timings'>
                        Sand Lake Road | Sunday to Tuesday 09.00 - 24.00
                    </span>
                    <span className='number'>+1-509-290-0179</span>
                </div>
                <div className='map'>
                    <div className='map-container'>
                        {isLoaded && (
                            <GoogleMap
                                mapContainerStyle={containerStyle}
                                center={center}
                                zoom={15}
                            />
                        )}
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Location;
