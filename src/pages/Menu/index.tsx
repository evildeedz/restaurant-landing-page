import { FC } from 'react';
import { Parallax } from 'react-parallax';
import menuImg from '../../assets/images/menu.jpg';
import menuList from '../../lists/menu';

interface IMenuProps {}

const Menu: FC<IMenuProps> = (props) => {
    return (
        <section id='menu'>
            <Parallax bgImage={menuImg} strength={300}>
                <div className='menu-container'>
                    <div className='container'>
                        <h2>Lunch & Dinner</h2>
                        <h4>Hot & Ready To Be Served</h4>
                        <div className='menu-list'>
                            {menuList.map((element, index) => (
                                <div key={index} className='menu-item'>
                                    <span className='item-name'>{element.name}</span>
                                    <span className='item-desc'>{element.desc}</span>
                                    <span className='item-info'>
                                        {element.weight} / ${element.price}
                                    </span>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </Parallax>
        </section>
    );
};

export default Menu;
