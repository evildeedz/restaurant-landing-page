import { FC } from 'react';
import galleryList from '../../lists/gallery';
import { GiVineFlower } from 'react-icons/gi';

interface IGalleryProps {}

const Gallery: FC<IGalleryProps> = (props) => {
    return (
        <section id='gallery'>
            <div className='container'>
                <h2>Gallery</h2>
                <GiVineFlower className='flower-icon' />
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
                    lobortis mauris ex, quis rutrum augue eleifend sed. Class aptent
                    taciti sociosqu ad litora torquent per conubia nostra, per inceptos
                    himenaeos. Maecenas in purus dui. Donec sed neque elit.
                </p>
                <div className='image-holder'>
                    {galleryList.map(({ src, imgTitle, subtitle }, index) => (
                        <div className='image' key={index}>
                            <img src={src} alt='...' />
                            <span>{imgTitle}</span>
                            <p>{subtitle}</p>
                        </div>
                    ))}
                </div>
            </div>
        </section>
    );
};

export default Gallery;
