import { FC } from 'react';
import { Parallax } from 'react-parallax';
import quoteImg from '../../assets/images/quote.jpg';
import { FaQuoteLeft } from 'react-icons/fa';
const Slide = require('react-reveal/Slide');

interface IQuoteProps {}

const Quote: FC<IQuoteProps> = (props) => {
    return (
        <section id='quote'>
            <Parallax bgImage={quoteImg} strength={200}>
                <div className='quote-container'>
                    <FaQuoteLeft className='quote-icon' />
                    <Slide duratioon={2000} bottom>
                        <span className='quote-text'>
                            PART OF THE SECRET OF SUCCESS IN LIFE IS TO EAT WHAT YOU LIKE.
                        </span>
                    </Slide>
                    <span className='chef-name'>Jeff Surkiz - Chef</span>
                </div>
            </Parallax>
        </section>
    );
};

export default Quote;
